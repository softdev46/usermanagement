/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.usermanagement;

/**
 *
 * @author ADMIN
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService.addUser("user2", "password");
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user3", "password"));
        System.out.println(UserService.getUsers());
        
        User user = UserService.getUser(3);
        System.out.println(user);
        user.setPassword("1234");
        UserService.updateUser(3, user);
        System.out.println(UserService.getUsers());
        
        UserService.delUser(user);
        System.out.println(UserService.getUsers());
        
        System.out.println(UserService.login("admin", "password"));
    }
}
