/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    // Mockup
    static {
        userList.add(new User("admin", "password"));
        userList.add(new User("user1", "password"));
    }

    // Create
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    // Overloading
    public static boolean addUser(String userName, String password) {
        userList.add(new User(userName, password));
        return true;
    }

    // Update User
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    // Read 1 user
    public static User getUser(int index) {
        if (index > userList.size()-1) {
            return null;
        }
        return userList.get(index);
    }

    // Read all user
    public static ArrayList<User> getUsers() {
        return userList;
    }

    // Search user name
    public static ArrayList<User> searchUserName(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return list;
    }
    
    // Delete user @index
    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }
    
    // Delete user @user
    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }
    
    // Login
    public static User login(String userName, String password){
        for (User user : userList) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)){
                return user;
            }
        }
        return null;
    }

}
